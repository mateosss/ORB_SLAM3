// Copyright 2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "Thirdparty/monado/slam_tracker.hpp"

#include <System.h>
#include <stdio.h>

#include <chrono>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include "Converter.h"
#include "Thirdparty/moodycamel/blockingconcurrentqueue.h"
#include "Thirdparty/moodycamel/concurrentqueue.h"
#include "Thirdparty/moodycamel/readerwriterqueue.h"

#if defined(_WIN32) || defined(__CYGWIN__)
#define EXPORT __declspec(dllexport)
#else
#define EXPORT __attribute__((visibility("default")))
#endif

namespace xrt::auxiliary::tracking::slam {

EXPORT extern const int IMPLEMENTATION_VERSION_MAJOR = HEADER_VERSION_MAJOR;
EXPORT extern const int IMPLEMENTATION_VERSION_MINOR = HEADER_VERSION_MINOR;
EXPORT extern const int IMPLEMENTATION_VERSION_PATCH = HEADER_VERSION_PATCH;

constexpr size_t IMU_QUEUE_INITIAL_SIZE = 128;
constexpr size_t FRAME_QUEUE_INITIAL_SIZE = 16;
constexpr size_t POSE_QUEUE_INITIAL_SIZE = 128;

using namespace ORB_SLAM3;

// We are not using the single producer - single consumer queue from moodycamel
// just because it does not support the try_dequeue_bulk operation, but
// it would be totally valid to use that instead.
using moodycamel::BlockingConcurrentQueue;
using moodycamel::ConcurrentQueue;
using moodycamel::ReaderWriterQueue;
using Sophus::SE3f;
using std::int64_t;
using std::shared_ptr;
using std::string;
using std::unique_ptr;
using std::vector;

#define printc(expr) std::cout << "\033[38;5;206m>>> " << #expr " = " << expr << "\033[0m\n";

#define ASSERT(cond, ...)                                                                                              \
  do {                                                                                                                 \
    if (!(cond)) {                                                                                                     \
      printf("Assertion failed @%s:%d\n", __func__, __LINE__);                                                         \
      printf(__VA_ARGS__);                                                                                             \
      printf("\n");                                                                                                    \
      exit(EXIT_FAILURE);                                                                                              \
    }                                                                                                                  \
  } while (false);
#define ASSERT_(cond) ASSERT(cond, "%s", #cond);

static void dump_imus(vector<IMU::Point> imus) {
  printf("dump imus.size()=%zu\n", imus.size());
  for (size_t i = 0; i < imus.size(); i++) {
    auto a = imus[i].a;
    auto w = imus[i].w;
    printf("\tt=%.17lf a=[%.17f, %.17f, %.17f] w=[%.17f, %.17f, %.17f]\n", imus[i].t, a.x(), a.y(), a.z(), w.x(), w.y(),
           w.z());
  }
}

static string sensor_to_str(System::eSensor sensor) {
  if (sensor == System::eSensor::MONOCULAR) {
    return "Monocular";
  } else if (sensor == System::eSensor::STEREO) {
    return "Stereo";
  } else if (sensor == System::eSensor::RGBD) {
    return "RGB-D";
  } else if (sensor == System::eSensor::IMU_MONOCULAR) {
    return "Monocular-Inertial";
  } else if (sensor == System::eSensor::IMU_STEREO) {
    return "Stereo-Inertial";
  } else {
    ASSERT(false, "Invalid SLAM type = %d", (int)sensor);
    return "Invalid";
  }
}

static vector<string> timing_titles{
    "frame_ts",      "tracker_received", "frames_received", "about_to_process",
    "pose_produced", "pose_pushed",      "monado_dequeued",
};

struct slam_tracker::implementation {
  using timed_img_sample = pair<timestats, img_sample>;
  using timed_pose = pair<timestats, pose>;

  unique_ptr<System> orbslam3;
  bool is_stereo;
  bool rectify;
  size_t fps;

  // Threading
  std::thread frame_consumer;
  std::mutex is_running_mutex;
  bool _is_running = false;

  // Queues
  BlockingConcurrentQueue<timed_img_sample> unsent_lframes{FRAME_QUEUE_INITIAL_SIZE};
  BlockingConcurrentQueue<img_sample> unsent_rframes{FRAME_QUEUE_INITIAL_SIZE}; // Unused on mono run
  ConcurrentQueue<imu_sample> unsent_imus{IMU_QUEUE_INITIAL_SIZE};
  ReaderWriterQueue<timed_pose> tracked_poses{POSE_QUEUE_INITIAL_SIZE};
  vector<imu_sample> imuvs{};
  vector<IMU::Point> imuvp{};

  // slam_tracker features
  unordered_set<int> supported_features{F_ENABLE_POSE_EXT_TIMING};
  bool pose_timing_enabled = false;

  // Rectification matrices
  cv::Mat M1l, M2l, M1r, M2r;

  implementation(string config_file) {
    cv::FileStorage config(config_file, cv::FileStorage::READ);
    if (!config.isOpened()) {
      cerr << "Error: wrong settings path: " << config_file << endl;
      ASSERT_(false);
      return;
    }

    string vocabulary = config["SLAM.vocabulary"];
    int slam_type_int = config["SLAM.type"];
    System::eSensor slam_type = (System::eSensor)slam_type_int;
    int visualize_int = config["SLAM.visualize"];
    bool visualize = visualize_int != 0;
    int rectify_int = config["SLAM.rectify"];
    int camera_fps = config["Camera.fps"];

    std::cout << "ORB-SLAM3 Wrapper for Monado will use the following config values:\n";
    std::cout << "Vocabulary file: " << vocabulary << "\n";
    std::cout << "System visualization: " << visualize << "\n";
    std::cout << "Apply rectification: " << rectify_int << "\n";
    std::cout << "SLAM type: " << sensor_to_str(slam_type) << "\n";
    std::cout << "Camera FPS: " << camera_fps << "\n";

    imuvs.reserve(IMU_QUEUE_INITIAL_SIZE * 2);
    imuvp.reserve(IMU_QUEUE_INITIAL_SIZE * 2);
    orbslam3 = unique_ptr<System>{new System(vocabulary, config_file, slam_type, visualize)};
    is_stereo = slam_type == System::eSensor::IMU_STEREO || slam_type == System::eSensor::STEREO;
    fps = camera_fps;
    rectify = rectify_int != 0;
    if (rectify) {
      init_rectification_matrices(config);
    }
  }

  void init_rectification_matrices(const cv::FileStorage &config) {
    // Taken from stereo_inertial_euroc.cc, wasn't tested with a camera that needed it
    cv::Mat K_l, K_r, P_l, P_r, R_l, R_r, D_l, D_r;
    config["LEFT.K"] >> K_l;
    config["RIGHT.K"] >> K_r;
    config["LEFT.P"] >> P_l;
    config["RIGHT.P"] >> P_r;
    config["LEFT.R"] >> R_l;
    config["RIGHT.R"] >> R_r;
    config["LEFT.D"] >> D_l;
    config["RIGHT.D"] >> D_r;

    int rows_l = config["LEFT.height"];
    int cols_l = config["LEFT.width"];
    int rows_r = config["RIGHT.height"];
    int cols_r = config["RIGHT.width"];

    bool invalid_params = K_l.empty() || K_r.empty() || P_l.empty() || P_r.empty() || R_l.empty() || R_r.empty() ||
                          D_l.empty() || D_r.empty() || rows_l == 0 || rows_r == 0 || cols_l == 0 || cols_r == 0;
    ASSERT(!invalid_params, "Calibration parameters for stereo rectification are missing");

    cv::Mat P3_l = P_l.rowRange(0, 3).colRange(0, 3);
    cv::Mat P3_r = P_r.rowRange(0, 3).colRange(0, 3);
    cv::initUndistortRectifyMap(K_l, D_l, R_l, P3_l, cv::Size(cols_l, rows_l), CV_32F, M1l, M2l);
    cv::initUndistortRectifyMap(K_r, D_r, R_r, P3_r, cv::Size(cols_r, rows_r), CV_32F, M1r, M2r);
  }

  void initialize() {}

  void start() {
    std::lock_guard<std::mutex> lock(is_running_mutex);
    _is_running = true;

    const auto &consumer_thread = is_stereo ? &slam_tracker::implementation::frame_consumer_stereo
                                            : &slam_tracker::implementation::frame_consumer_mono;

    frame_consumer = std::thread(consumer_thread, this);
  }

  void stop() {
    is_running_mutex.lock();
    orbslam3->Shutdown();
    _is_running = false;
    is_running_mutex.unlock();

    frame_consumer.join();
  }

  void finalize() {}

  bool is_running() {
    std::lock_guard<std::mutex> lock(is_running_mutex);
    return _is_running;
  }

  void push_imu_sample(imu_sample s) { unsent_imus.enqueue(s); }

  void push_frame(img_sample s) {
    if (s.cam_index == 0) {
      timestats tk{};
      tk.timing_enabled = pose_timing_enabled;
      tk.timing.reserve(timing_titles.size());
      tk.timing_titles = &timing_titles;
      tk.addTime("frame_ts", s.timestamp);
      tk.addTime("tracker_received");
      unsent_lframes.enqueue({tk, s});
    } else {
      ASSERT_(is_stereo);
      unsent_rframes.enqueue(s);
    }
  }

  bool try_dequeue_pose(pose &pose) {
    timed_pose tp;
    bool dequeued = tracked_poses.try_dequeue(tp);
    if (dequeued) {
      timestats &tk = tp.first;
      pose = tp.second;

      tk.addTime("monado_dequeued");
      if (tk.timing_enabled) {
        pose_ext_timing_data petd = tk;
        auto pose_timing = make_shared<pose_ext_timing>(petd);
        pose.next = pose_timing;
      }
    }
    return dequeued;
  }

  bool supports_feature(int feature_id) { return supported_features.count(feature_id) == 1; }

  bool use_feature(int feature_id, const shared_ptr<void> &params, shared_ptr<void> &result) {
    result = nullptr;
    if (feature_id == FID_EPET) {
      shared_ptr<FPARAMS_EPET> casted_params = static_pointer_cast<FPARAMS_EPET>(params);
      result = enable_pose_ext_timing(*casted_params);
    } else {
      return false;
    }
    return true;
#if 0 // This is how this should look if we supported other features, but we don't
    if (feature_id == FID_ACC) {
      shared_ptr<FPARAMS_ACC> casted_params = static_pointer_cast<FPARAMS_ACC>(params);
      add_cam_calibration(*casted_params);
    } else if (feature_id == FID_AIC) {
      shared_ptr<FPARAMS_AIC> casted_params = static_pointer_cast<FPARAMS_AIC>(params);
      add_imu_calibration(*casted_params);
    } else {
      return false;
    }
    return true;
#endif
  }
  shared_ptr<vector<string>> enable_pose_ext_timing(bool enable) {
    pose_timing_enabled = enable;
    return make_shared<vector<string>>(timing_titles);
  }

  vector<IMU::Point> get_unsent_imus_upto(double ts) {
    size_t step = IMU_QUEUE_INITIAL_SIZE;

    // Dequeue all imu_samples into imuvs
    imuvs.clear();
    size_t dequeued = UINT64_MAX;
    size_t total_dequeued = 0;
    do {
      imuvs.resize(imuvs.size() + step);
      dequeued = unsent_imus.try_dequeue_bulk(imuvs.begin() + total_dequeued, step);
      total_dequeued += dequeued;
    } while (dequeued >= step);
    imuvs.resize(total_dequeued);

    // Convert them to IMU::Points and push them to imuvp
    for (size_t i = 0; i < dequeued; i++) {
      const imu_sample &s = imuvs[i];
      double ts = s.timestamp / 1e9;
      auto imup = IMU::Point{(float)s.ax, (float)s.ay, (float)s.az, (float)s.wx, (float)s.wy, (float)s.wz, ts};
      imuvp.push_back(imup);
    }

    // Find index i just after ts, assumes imuvp is ordered
    size_t i;
    for (i = 0; i < imuvp.size(); i++) {
      if (imuvp[i].t > ts) {
        break;
      }
    }

    vector<IMU::Point> unsent_imus_upto_ts{imuvp.begin(), imuvp.begin() + i};
    imuvp.erase(imuvp.begin(), imuvp.begin() + i); // Keep samples greater than ts
    return unsent_imus_upto_ts;
  }

  void try_push_pose(timestats &tk, int64_t ts, SE3f T_c_w) {

    SE3f T_w_c = T_c_w.inverse();
    SE3f T_c_b = orbslam3->settings_->Tbc().inverse(); // Identity if no IMU present
    SE3f T_w_b = T_w_c * T_c_b;
    Eigen::Vector3f p = T_w_b.translation();
    Eigen::Quaternionf r = T_w_b.unit_quaternion();

    pose pose{ts, p.x(), p.y(), p.z(), r.x(), r.y(), r.z(), r.w()};
    tk.addTime("pose_pushed");
    tracked_poses.enqueue({tk, pose});
  }

  void process_frame(timed_img_sample &timed_sample) {
    // NOTE: On monocular example:
    // There is only one IMU sample befoore the first frame
    // Only IMU samples greater than previous frame ts and lower or equal to
    // current frame ts are pushed
    timestats &tk = timed_sample.first;
    const img_sample &sample = timed_sample.second;

    double ts = sample.timestamp / 1e9; // ns to seconds
    tk.addTime("about_to_process");
    SE3f pose = orbslam3->TrackMonocular(sample.img, ts, get_unsent_imus_upto(ts));
    tk.addTime("pose_produced");
    try_push_pose(tk, sample.timestamp, pose);
  }

  void process_stereo_frames(timed_img_sample &timed_lsample, img_sample rsample) {
    timestats &tk = timed_lsample.first;
    const img_sample &lsample = timed_lsample.second;

    double ts_l = lsample.timestamp / 1e9; // ns to seconds
    double ts_r = rsample.timestamp / 1e9;

    ASSERT(ts_l == ts_r, "Unsynced stereo timestamps ts_l=%lf ts_r=%lf", ts_l, ts_r);

    cv::Mat left, right;
    if (rectify) {
      cv::remap(lsample.img, left, M1l, M2l, cv::INTER_LINEAR);
      cv::remap(rsample.img, right, M1r, M2r, cv::INTER_LINEAR);
    } else {
      left = lsample.img;
      right = rsample.img;
    }

    tk.addTime("about_to_process");
    SE3f pose = orbslam3->TrackStereo(left, right, ts_l, get_unsent_imus_upto(ts_l));
    tk.addTime("pose_produced");
    try_push_pose(tk, lsample.timestamp, pose);
  }

  void frame_consumer_mono() {
    std::unique_lock<std::mutex> lock(is_running_mutex);
    int64_t three_frames_ms = (1.0 / fps) * 1000 * 3;
    auto timeout = std::chrono::milliseconds(three_frames_ms);

    while (_is_running) {
      lock.unlock();
      timed_img_sample timed_frame{};
      bool dequeued = unsent_lframes.wait_dequeue_timed(timed_frame, timeout);
      if (dequeued) {
        timed_frame.first.addTime("frames_received");
        process_frame(timed_frame);
      }
      lock.lock();
    }
  }

  void frame_consumer_stereo() {

    std::unique_lock<std::mutex> lock(is_running_mutex, std::defer_lock);
    int64_t three_frames_ms = (1.0 / fps) * 1000 * 3;
    auto timeout = std::chrono::milliseconds(three_frames_ms);
    timed_img_sample timed_lframe;
    img_sample rframe{};
    bool ldequeued = false;
    bool rdequeued = false;

    while (lock.lock(), _is_running) {
      lock.unlock();
      if (!ldequeued) {
        ldequeued = unsent_lframes.wait_dequeue_timed(timed_lframe, timeout / 2);
      }

      if (!ldequeued) {
        continue;
      }

      ASSERT_(ldequeued);

      rdequeued = unsent_rframes.wait_dequeue_timed(rframe, timeout / 2);
      if (!rdequeued) {
        continue;
      }

      ASSERT_(ldequeued && rdequeued);

      timed_lframe.first.addTime("frames_received");
      process_stereo_frames(timed_lframe, rframe);
      ldequeued = rdequeued = false;
    }
  }
};

EXPORT slam_tracker::slam_tracker(const slam_config &config) {
  using Impl = slam_tracker::implementation;
  ASSERT(config.cam_count == 2 || config.cam_count == 1, "cam_count=%d", config.cam_count);
  impl = unique_ptr<Impl>(new Impl{*config.config_file});
}

EXPORT slam_tracker::~slam_tracker() = default;

EXPORT void slam_tracker::initialize() { impl->initialize(); }

EXPORT void slam_tracker::start() { impl->start(); }

EXPORT void slam_tracker::stop() { impl->stop(); }

EXPORT void slam_tracker::finalize() { impl->finalize(); }

EXPORT bool slam_tracker::is_running() { return impl->is_running(); }

EXPORT void slam_tracker::push_imu_sample(const imu_sample &s) { impl->push_imu_sample(s); }

EXPORT void slam_tracker::push_frame(const img_sample &sample) { impl->push_frame(sample); }

EXPORT bool slam_tracker::try_dequeue_pose(pose &pose) { return impl->try_dequeue_pose(pose); }

EXPORT bool slam_tracker::supports_feature(int feature_id) { return impl->supports_feature(feature_id); }

EXPORT bool slam_tracker::use_feature(int feature_id, const shared_ptr<void> &params, shared_ptr<void> &result) {
  return impl->use_feature(feature_id, params, result);
}

} // namespace xrt::auxiliary::tracking::slam
