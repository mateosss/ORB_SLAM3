# ORB-SLAM3 for Monado

This is a fork of [ORB-SLAM3](https://github.com/UZ-SLAMLab/ORB_SLAM3) with some
modifications so that it can be used from Monado for SLAM tracking. Many thanks
to the ORB-SLAM3 authors.

## Index

- [ORB-SLAM3 for Monado](#orb-slam3-for-monado)
  - [Index](#index)
  - [Installation](#installation)
    - [Build and Install Directories](#build-and-install-directories)
    - [Dependencies](#dependencies)
    - [Build ORB-SLAM3](#build-orb-slam3)
    - [Monado Specifics](#monado-specifics)
  - [Using a RealSense Camera](#using-a-realsense-camera)
    - [Overview of the Setup (D455)](#overview-of-the-setup-d455)
      - [SLAM-Tracked RealSense Driver](#slam-tracked-realsense-driver)
      - [RealSense-Tracked Qwerty Driver](#realsense-tracked-qwerty-driver)
    - [Non-D455 RealSense Devices](#non-d455-realsense-devices)
      - [Configuring the RealSense Pipeline](#configuring-the-realsense-pipeline)
      - [Configuring ORB-SLAM3](#configuring-orb-slam3)
    - [Notes on ORB-SLAM3 Usage](#notes-on-orb-slam3-usage)

## Installation

This was tested on both Ubuntu 20.04 and 18.04, be sure to open an issue if the
steps don't work for you.

### Build and Install Directories

To not clutter your system directories, let's set two environment variables,
`$orb3deps` and `$orb3install` that point to existing empty build and install
directories respectively. These directories will contain everything produced in
this guide.

```bash
# Change the paths accordingly
export orb3install=/home/mateo/Documents/apps/orb3install
export orb3deps=/home/mateo/Documents/apps/orb3deps
```

Let's extend our system paths with those.

```bash
export PATH=$orb3install/bin:$PATH
export PKG_CONFIG_PATH=$orb3install/lib/pkgconfig:$PKG_CONFIG_PATH # for compile time pkg-config
export LD_LIBRARY_PATH=$orb3install/lib/:$LD_LIBRARY_PATH # for runtime ld
export LIBRARY_PATH=$orb3install/lib/:$LIBRARY_PATH # for compile time gcc
```

### Dependencies

_Note: if any dependency is missing, please take a look at the compilation guide
from the original [ORB-SLAM3](https://github.com/UZ-SLAMLab/ORB_SLAM3) repo_

- Eigen3 >= 3.1.0: `sudo apt install libeigen3-dev`

- OpenCV >= 4.4.0: Ubuntu 20.04 has 4.2.0 on `libopencv-dev` so let's compile it ourselves:

  ```bash
  cd $orb3deps
  wget https://github.com/opencv/opencv/archive/4.4.0.zip
  unzip 4.4.0.zip
  cd opencv-4.4.0/ && mkdir build && cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=$orb3install
  cmake --build . --target install -- -j $(nproc)
  ```

- Pangolin:
  ```bash
  cd $orb3deps
  sudo apt install libgl1-mesa-dev libglew-dev cmake pkg-config
  git clone https://github.com/stevenlovegrove/Pangolin.git
  cd Pangolin && mkdir build && cd build
  git checkout v0.6 # Newer versions unsupported
  cmake .. -DCMAKE_INSTALL_PREFIX=$orb3install -DCMAKE_CXX_FLAGS=-march=native
  cmake --build . --target install -- -j $(nproc)
  ```

### Build ORB-SLAM3

```bash
cd $orb3deps
git clone git@gitlab.freedesktop.org:mateosss/ORB_SLAM3.git
cd ORB_SLAM3
sed -i "s#/home/mateo/Documents/apps/orb3deps/#$orb3deps/#" Examples/**/*.yaml
sh build.sh $orb3install # This will also install DBoW2, g2o and the vocabulary file
```

### Monado Specifics

For now, you will need to build Monado from my
[mateosss/orbslam3](https://gitlab.freedesktop.org/mateosss/monado/-/tree/mateosss/orbslam3)
branch (we only need minor changes from its last commits really).

Before building monado do `export CXXFLAGS=-march=native CFLAGS=-march=native`
(because we've been building everything with `-march=native` until this point and
not doing so might result in weird crashes related to Eigen)

Run an OpenXR app like `hello_xr` with the following environment variables set

```bash
export EUROC_PATH=/path/to/euroc/V1_01_easy/ # Set euroc dataset path. You can get a dataset from http://robotics.ethz.ch/~asl-datasets/ijrr_euroc_mav_dataset/vicon_room1/V1_01_easy/V1_01_easy.zip
export EUROC_LOG=debug
export EUROC_HMD=false # if false, a fake controller will be tracked, else a fake HMD
export SLAM_LOG=debug
export SLAM_CONFIG=$orb3deps/ORB_SLAM3/Examples/Stereo-Inertial/EuRoC.yaml # Point to ORB_SLAM3 config file for stereo-inertial Euroc
export OXR_DEBUG_GUI=1 # We will need the debug ui to start streaming the dataset
```

Finally, run the XR app and press start in the euroc player debug ui and you
should see a controller being tracked with ORB-SLAM3 from the euroc dataset

## Using a RealSense Camera

After making sure that everything works by running the EuRoC datasets, it should
be possible to use the `realsense` driver from Monado to get any RealSense
camera that has an IMU and one or more cameras to get tracked with SLAM.
However, this was only tested on a D455, so if you are having problems with
another device, please open an issue. Also, open an issue if you manage to make
it work with other devices so that I can add it to this README.

### Overview of the Setup (D455)

Let's first assume you have a RealSense D455, which is the one that works with
the defaults. Even if you have another RealSense device follow this section, you
might at least get something working, although not at its best.

#### SLAM-Tracked RealSense Driver

Set these environment variables:

- `export RS_HDEV_LOG=debug`: Make our realsense device logs more verbose
- `export RS_SOURCE_INDEX=0`: Indicate that we want to use the first RealSense device connected as data source
- `export RS_TRACKING=2`: Only try to use "host-slam". See other options
  [here](https://gitlab.freedesktop.org/mateosss/monado/-/blob/64e70e76ad6d47e4bd1a0dfa164bff8597a50ce8/src/xrt/drivers/realsense/rs_prober.c#L33-39).
- `export SLAM_CONFIG=$orb3deps/ORB_SLAM3/Examples/Stereo-Inertial/D455.yaml`:
  Configuration file for ORB-SLAM3 and the D455 in stereo-inertial mode.

#### RealSense-Tracked Qwerty Driver

You now have a RealSense device that you can use to track another device, for
example, let's track a Qwerty HMD.

Set these environment variables to enable the qwerty driver:

```bash
export QWERTY_ENABLE=true QWERTY_COMBINE=true
```

And then modify your tracking overrides in your monado configuration file
(`~/.config/monado/config_v0.json`) by updating the json object with:

```js
{
  "tracking": {
    "tracking_overrides": [
      {
        "target_device_serial": "Qwerty HMD", // Or "Qwerty Left Controller"
        "tracker_device_serial": "Intel RealSense Host-SLAM",
        "type": "direct",
        "offset": {
          "orientation": { "x": 0, "y": 0, "z": 0, "w": 1 },
          "position": { "x": 0, "y": 0, "z": 0 }
        },
        "xrt_input_name": "XRT_INPUT_GENERIC_TRACKER_POSE"
      }
    ],
  }
}
```

And that's it! You can now start an OpenXR application with Monado and get your
view tracked with your D455 camera.

### Non-D455 RealSense Devices

While I was unable to test other devices because I don't have access to them, it
should be possible to make them work by:

#### Configuring the RealSense Pipeline

[These
fields](https://gitlab.freedesktop.org/mateosss/monado/-/blob/9e1b7e2203ef49abb939cc8fc92afa16fcc9cb3a/src/xrt/drivers/realsense/rs_hdev.c#L118-129)
determine your RealSense streaming configuration, and
[these](https://gitlab.freedesktop.org/mateosss/monado/-/blob/9e1b7e2203ef49abb939cc8fc92afa16fcc9cb3a/src/xrt/drivers/realsense/rs_hdev.c#L40-50)
are their current defaults that work on a D455. You can change those fields by
setting any of them in your `config_v0.json` inside a `config_realsense_hdev`
field. Also note that as we already set `RS_HDEV_LOG=debug`, you should see the
values they are currently taking at the start of Monado.

For example, let's say you have a realsense device which has two fisheye cameras
that support streaming 640x360 at 30fps (a T265 I think), then a configuration
like this should work:

```js
"realsense_config_hdev": {
  "stereo": true,
  "video_format": 9, // 9 gets casted to RS2_FORMAT_Y8 (see https://git.io/Jzkfw), grayscale
  "video_width": 640, // I am assuming the T265 supports 640x360 streams at 30fps
  "video_height": 360,
  "video_fps": 30,
  "gyro_fps": 0, // 0 indicates any
  "accel_fps": 0,
  "stream_type": 4, // 4 gets casted to RS2_STREAM_FISHEYE (see https://git.io/Jzkvq)
  "stream1_index": -1, // If there were more than one possible stream with these properties select them, -1 is for auto
  "stream2_index": -1,
}
```

The particular values you could set here are very dependant on your camera. I
recommend seeing the values that get output by running the [rs-sensor-control
example](https://dev.intelrealsense.com/docs/rs-sensor-control) from the
RealSense API.

#### Configuring ORB-SLAM3

As you might've noticed, we set `SLAM_CONFIG` to
`$orb3deps/ORB_SLAM3/Examples/Stereo-Inertial/D455.yaml` which is
[this](Examples/Stereo-Inertial/D455.yaml) config file that I added for the D455
stereo-inertial configuration.

For the tracking to be as good as possible you should set the
intrinsics/extrinsics of the device in a similar config file and point to it
with `SLAM_CONFIG`. You can obtain that information from the previously
mentioned
[rs-sensor-control](https://dev.intelrealsense.com/docs/rs-sensor-control)
utility. Although there are many issues in the original ORB_SLAM3 repository
where people share their configuration files, like [this
(T265)](https://github.com/UZ-SLAMLab/ORB_SLAM3/issues/347#issuecomment-875315170)
or [this
(D435)](https://github.com/UZ-SLAMLab/ORB_SLAM3/issues/284#issuecomment-817145966).

### Notes on ORB-SLAM3 Usage

- For now, this fork is an initial approximation into using ORB-SLAM3 with
  Monado. The tracking is not perfect, [here](https://youtu.be/kJwWY973b10) is
  an example of how it should look. (Notice that while it handles quite fast
  movements, at the end of the video it starts drifting away because of those).
- ORB-SLAM3 can be run with only one camera, or with two cameras and IMU, and
  everything in between. Set `SLAM.type` appropriately in the `SLAM_CONFIG` file.
- As RealSense devices usually give already rectified images you can keep
  `SLAM.rectify=0` and don't need to set `LEFT.*` and `RIGHT.*` in the
  `SLAM_CONFIG` file. For fisheye cameras see [this
  answer](https://github.com/UZ-SLAMLab/ORB_SLAM3/issues/88#issuecomment-684658487).
- When using IMU configurations, you need to excite all the sensors in a calm
  way for around 15 seconds. The coordinate system will change once or twice,
  and after that everything starts working fine.
- If you are not using an IMU configuration, then the pose returned in Monado
  might be wrongly corrected, checkout `rs_hdev_correct_pose_from_orbslam3`
  comments in Monado for a solution.
