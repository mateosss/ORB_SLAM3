set -e

if [ $# -eq 0 ]; then
    echo "No arguments received. Usage: ./build.sh INSTALL_PREFIX [BUILD_TYPE] [BUILD_EXAMPLES]"
    exit 1
fi

INSTALL_PREFIX=$1
BUILD_TYPE=${2:-RelWithDebInfo}
BUILD_EXAMPLES=${3:-OFF}
echo "Using -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX"
echo "Using -DCMAKE_BUILD_TYPE=$BUILD_TYPE"
echo "Using -DBUILD_EXAMPLES=$BUILD_EXAMPLES"

echo "Configuring and building Thirdparty/DBoW2 ..."
cd Thirdparty/DBoW2
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$1 -DCMAKE_EXPORT_COMPILE_COMMANDS=on
make -j $(nproc) install
cd ../../g2o

echo "Configuring and building Thirdparty/g2o ..."
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$1 -DCMAKE_EXPORT_COMPILE_COMMANDS=on
make -j $(nproc) install

cd ../../Sophus

echo "Configuring and building Thirdparty/Sophus ..."

mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$1 -DCMAKE_EXPORT_COMPILE_COMMANDS=on
make -j $(nproc) install

cd ../../../

echo "Uncompress vocabulary ..."
cd Vocabulary
tar -xf ORBvoc.txt.tar.gz
cd ..

echo "Configuring and building ORB_SLAM3 ..."
mkdir -p build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$1 -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_EXPORT_COMPILE_COMMANDS=On -DBUILD_EXAMPLES=$BUILD_EXAMPLES
make -j $(nproc) install
